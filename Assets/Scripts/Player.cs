﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MovingObject {

	public Text healthText;
	public AudioClip movementSound1;
	public AudioClip movementSound2;
	public AudioClip chopSound1;
	public AudioClip chopSound2;
	public AudioClip fruitSound1;
	public AudioClip fruitSound2;
	public AudioClip sodaSound1;
	public AudioClip sodaSound2;

	private Animator animator;
	private int playerHealth;
	private int attackPower = 1;
	private int healthPerFruit = 5;
	private int healthPerSoda = 10;
	private int healthPerCoin= 20;
	private int secondsUntilnextLevel = 1;

	private int minScreenWidth;
	private int maxScreenWidth;

	protected override void Start()
	{
		base.Start();
		animator = GetComponent<Animator>();
		playerHealth = GameController.Instance.playerCurrentHealth;
		healthText.text = "Health: " + playerHealth;
	}

	private void OnDisable()
	{
		GameController.Instance.playerCurrentHealth = playerHealth;
	}

	void Update () {
	
		if (!GameController.Instance.isPlayerTurn) {
			return;
		}
		CheckIfGameOver();
		int xAxis = 0;
		int yAxis = 0;


		if (Input.touchCount > 0) {
			Vector2 touchPosition = Input.GetTouch(0).position;
			xAxis = (int)touchPosition.x;
			yAxis = (int)touchPosition.y;

			GetScreenSize();
			if(xAxis >= minScreenWidth && xAxis <= maxScreenWidth){
				ExecuteTouch();
			}else{
				xAxis = 0;
				yAxis = 0;
			}

		} else {
			xAxis = (int)Input.GetAxisRaw ("Horizontal");
			yAxis = (int)Input.GetAxisRaw ("Vertical");

			if (xAxis != 0) {
				yAxis = 0;
			}
		}

		if (xAxis != 0 || yAxis != 0) {
			playerHealth--;
			healthText.text = "Health: " + playerHealth;
			SoundController.Instance.PlaySingle(movementSound1, movementSound2);
			Move<Wall>(xAxis, yAxis);
			GameController.Instance.isPlayerTurn = false;
		}

	}


	private void GetScreenSize(){
	
	}
	private void ExecuteTouch(){
	
	} 

	private void OnTriggerEnter2D(Collider2D objectPlayerCollideWith)
	{
		if (objectPlayerCollideWith.tag == "Exit") {
			Invoke ("LoadNewLevel", secondsUntilnextLevel);
			enabled = false;
		} else if (objectPlayerCollideWith.tag == "Fruit") {
			playerHealth += healthPerFruit;
			healthText.text = "+" + healthPerFruit + " Health\n" + "Health: " + playerHealth;
			objectPlayerCollideWith.gameObject.SetActive (false);
			SoundController.Instance.PlaySingle(fruitSound1, fruitSound2);
		} else if (objectPlayerCollideWith.tag == "Soda") {
			playerHealth += healthPerSoda;
			healthText.text = "+" + healthPerSoda + " Health\n" + "Health: " + playerHealth;
			objectPlayerCollideWith.gameObject.SetActive (false);
			SoundController.Instance.PlaySingle(sodaSound1, sodaSound2);
		} else if (objectPlayerCollideWith.tag == "USD") {
			playerHealth += healthPerCoin;
			healthText.text = "+" + healthPerCoin + " Health\n" + "Health: " + playerHealth;
			objectPlayerCollideWith.gameObject.SetActive (false);
		}
	}
	private void LoadNewLevel()
	{
		Application.LoadLevel(Application.loadedLevel);
	}

	protected override void HandleCollision<T>(T component)
	{
		Wall wall = component as Wall;
		animator.SetTrigger("PlayerAttack");
		SoundController.Instance.PlaySingle(chopSound1, chopSound2);
		wall.DamageWall(attackPower);
	}

	public void TakeDamage(int damageReceived)
	{
		playerHealth -= damageReceived;
		healthText.text = "-" + damageReceived + " Health\n" + "Health: " + playerHealth;
		animator.SetTrigger("PlayerHurt");

	}
	private void CheckIfGameOver(){
		if (playerHealth <= 0) {
			GameController.Instance.GameOver();
		}
	}
}
